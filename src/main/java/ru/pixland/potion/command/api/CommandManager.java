package ru.pixland.potion.command.api;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.experimental.UtilityClass;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.Plugin;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Класс взят из kaytato api :>
 * (old: jamesapi)
 *
 * @author: vk.com/kaytato
 */
@UtilityClass
public class CommandManager {
    @Getter
    private final Collection<kCommand<?>> commandCollection = new ArrayList<>();
    private static CommandMap COMMAND_MAP;

    public void registerCommand(Plugin plugin, kCommand<?> baseCommand,
                                String command, String... aliases) {
        baseCommand.setLabel(command);
        baseCommand.setName(command);
        baseCommand.setAliases(Lists.newArrayList(aliases));
        commandCollection.add(baseCommand);
        try {
            if (COMMAND_MAP == null) {
                String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
                Class<?> craftServerClass = Class.forName("org.bukkit.craftbukkit." + version + ".CraftServer");
                Object craftServerObject = craftServerClass.cast((Object) Bukkit.getServer());
                Field commandMapField = craftServerClass.getDeclaredField("commandMap");
                commandMapField.setAccessible(true);

                COMMAND_MAP = (SimpleCommandMap) commandMapField.get(craftServerObject);
            }

            COMMAND_MAP.register(plugin.getName(), baseCommand);
        } catch (Exception e) {
        }
    }



}
