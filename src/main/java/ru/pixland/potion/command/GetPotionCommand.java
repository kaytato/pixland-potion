package ru.pixland.potion.command;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import ru.pixland.potion.command.api.kCommand;
import ru.pixland.potion.potion.Potion;

import java.util.*;

public class GetPotionCommand extends kCommand<Player> {

    public static final Map<Player, List<Player>> INFECT_MAP = new HashMap<>();

    public GetPotionCommand() {
        super("getpotion");
    }

    @Override
    protected void handleExecute(Player sender, String[] args) {
        Potion potion = Potion.newBuilder()
                .potionId("strengthening-voodoo")
                .potionName("Усиление вуду")
                .effect(new PotionEffect(PotionEffectType.LUCK, 90, 1))
                .onPlayerDamaged(event -> {

                    List<Player> list = INFECT_MAP.getOrDefault(sender, Arrays.asList(event.getTarget()));

                    if (Potion.POTION_PLAYER_MAP.get(event.getTarget()).getPotionName().equals("water-breathing")) {
                        if (event.getTarget().getActivePotionEffects().isEmpty()) {
                            list.remove(event.getTarget());
                            return;
                        }

                        double damage = event.getDamage() / list.size();
                        list.forEach(player -> player.damage(damage));
                        event.setCancelled(true);
                        return;
                    }

                    Potion targetPotion = Potion.newBuilder().potionId("water-breathing")
                            .potionName("Связывание души")
                            .effect(new PotionEffect(PotionEffectType.WATER_BREATHING, 90, 1))
                            .build();
                    targetPotion.infect(event.getTarget());

                    // todo: перепишите этот ужас за меня))))
                    list.add(event.getTarget());
                    INFECT_MAP.put(sender, list);
                }).build();
        potion.infect(sender);
    }
}
